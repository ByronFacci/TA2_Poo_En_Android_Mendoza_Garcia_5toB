package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    // Creamos las Instancias nombre, descripcion, precio e imagen

    TextView Nombre, DescripciOn, Precio;
    ImageView Imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Hacemos el casting de las instancias y especificamos sus ID

        Nombre= (TextView)findViewById(R.id.txtNombre);
        DescripciOn= (TextView)findViewById(R.id.txtDescripcion);
        Precio= (TextView)findViewById(R.id.txtPrecio);
        Imagen= (ImageView)findViewById(R.id.imagen);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        // INICIO - CODE6


        //Creamos una variable String object que sirve para recibir el object_id enviado desde la clase ResultActivity

        String object_id = getIntent().getExtras().getString("object_id");

        DataQuery dataQuery = DataQuery.get("item");
        // pasamos como parametro la Variable String creada anteriormente
        dataQuery.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){

                    //Creamos variables y accedemos a las propiedades nombre, precio, descripcion e imagenes
                    String nombre = (String) object.get("name");
                    String descripcion = (String) object.get("description");
                    String  precio = (String) object.get("price");
                    Bitmap imagen = (Bitmap)object.get("image");

                    Nombre.setText(nombre);
                    DescripciOn.setText(descripcion);
                    Precio.setText(precio + " " + getResources().getString(R.string.signodolar));
                    Imagen.setImageBitmap(imagen);



                }else {
                    //ERROR

                }

            }
        });


        // FIN - CODE6

    }

}
